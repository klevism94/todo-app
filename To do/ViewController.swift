//
//  ViewController.swift
//  To do
//
//  Created by Klevis Mehmeti on 30/12/16.
//  Copyright © 2016 Klevis Mehmeti. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDataSource {

    var todo = [ToDo]()
    
    @IBOutlet weak var tableView :UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        tableView.registerNib(UINib(nibName: "cellView",bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "toDoCell")
        
    }

    
    override func viewDidAppear(animated: Bool) {
        FetchAndSet()
        tableView.reloadData()
    }
    
   
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCellWithIdentifier("toDoCell") as! cellView
       let  list =  todo[indexPath.row]
        cell.labelTodo.text = list.texttodo
        return cell
        
    }

    
  
    func FetchAndSet(){
        let app = UIApplication.sharedApplication().delegate as? AppDelegate
        let context = app?.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName:"ToDo")
        
        do { let result = try context?.executeFetchRequest(fetchRequest)
            self.todo = result as! [ToDo]
        }
        catch let err as NSError {
            print("error with fetch")
        }
        
    }

}

