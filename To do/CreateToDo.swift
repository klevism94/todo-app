//
//  CreateToDo.swift
//  To do
//
//  Created by Klevis Mehmeti on 30/12/16.
//  Copyright © 2016 Klevis Mehmeti. All rights reserved.
//

import UIKit
import CoreData

class CreateToDo: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var imagePicker : UIImagePickerController!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageToDo: UIImageView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
        imageToDo.image = image
    }
    
    @IBAction func SaveClick(sender: AnyObject) {
        
        if let title = textField.text where title != "" {
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            let entity = NSEntityDescription.entityForName("ToDo", inManagedObjectContext: context)!
            let todo = ToDo(entity: entity,insertIntoManagedObjectContext : context)
            todo.texttodo = textField.text
            
            
            
            context.insertObject(todo)
            
            do {
                try context.save()
            }
                
            catch {print ("error context")}
        }
      self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func GetPhoto(sender: AnyObject) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
}
